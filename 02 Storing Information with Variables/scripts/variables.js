
/**
* Variables
* These are like boxes: can be filled, look for its contents and emptied. When
* the contents of the box change, it is still the same box. To identify each
* variable, it has a name.
*
* To create a create a variable in JavaScript, the special keyword `var` is used.
*
* var box_1;
*
* A value can be asigned to variable upon creation.
*
* var box_2 = 'some stuff';
*/

var message = 'Hello!';
alert(message);

message = 'Welcome to JavaScript Basics';
alert(message);

/**
* Naming Variables
* When giving names to variables one must follow these rules:
*  - Don't use the language reserved words.
*  - The name can't start with a number.
*  - Names can only be composed by letters, numbers, $ and underscores.
*
* Other conventions are optional but it is good to follow them to improve the
* readability of the code:
*
*  - Separate composed variable names with underscores or using camel case.
*  - Use desccriptive and specific names.
*/

/**
* Strings and Numbers
* Two common varible types in javascrip are strings and numbers.
* Numbers are used to store values to perform computations.
* Strings are used to store text, surrounded by quote marks, single or double.
* Double quotes can be used to surround strings that contain single quotes.
*
* var text = "It's quite nice!";
*
* Other option is to scape text single quotes.
*
* var text = 'It\'s quite nice!';
*/

/**
* Capturing Visitor Input
* JavaScript provides the `prompt` command to create a dialog message to capture
* the user input.
*/

var visitorName = prompt('What is your name?');
console.log(visitorName);

/**
* Combining Scripts
* Strings can be concatenated useing the `+` operator. When a variable contains
* string, other string can be appended and stored  to it by using the `+=`
* operator.
*/

var greet  = 'Welcome to Treehouse, ' + visitorName;
greet += "\nWe hope you learn a lot!";
alert(greet);

/**
* String Attributes
* 
*  - length: returns the number of charaters in a string.
*
* String Methods
*
*  - toLowerCase(): returns the lower case version of the string.
*  - toUpperCase(): returns the upper case version of the string.
*/

var stringtoShout = prompt('What should I shout?');
alert(stringtoShout.toUpperCase() + '!!!');