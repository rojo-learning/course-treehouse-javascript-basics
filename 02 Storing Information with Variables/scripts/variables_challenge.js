
/**
* Instructions
*
* 1. Create a variable capturer input.
* 2. Create a separate variable for each piece of input.
* 3. Add an alert to tell the visitor that they're finished.
* 4. Combine the input with other strings to create a message.
* 5. Print the story to the browser window.
*/

var name = prompt('Hello! What is your name?');

var story = "<div class='container'><p>Nice to meet you, " + name + ".</p>";
story += "<p>Welcome to Alola. This world is inhabited with creatures called \
          pokémon. Humans and pokémon share their lives, creating bonds \
          and achieving great feats.</p>";

document.write(story);

var type = prompt('What pokémon type is your favorite?');

story = "<p>On these islands, kids of your age start a travel trough each \
         island, attemping tests from the Kahuna that help them to learn \
         and growth in the company of their beloved pokemon.</p>";
story += "<p>If you participate, you may meet several pokémon of the " + type +
           " type. Ain't that cool?</p>"

document.write(story);

var region = prompt('From what region are you from?');

story = "<p>So you came all the way from " + region + ". You'll find that going \
        around Alola may seem simple if you don't have a boat, but once you \
        learn about Poké Ride, you'll be able to explore the region more \
        freely.</p>"
story += "<p>Good luck and enjoy your experiences along with Pokémon!</p></div>"

document.write(story);
