
var randomNumber = Math.floor(Math.random() * 6) + 1;
var first_guess = parseInt(prompt('I\'m thinking of a number between 1 and 6.\nWhat is it?'));

if (randomNumber == first_guess) {
  document.write('<p>Your guessed the number!</p>');
} else {
  if (randomNumber < first_guess) {
    second_guess = parseInt(prompt('Try again, the number is lower.'));
  } else {
    second_guess = parseInt(prompt('Try again, the number is higher.'));
  }
}

if (randomNumber == second_guess) {
  document.write('<p>Your guessed the number!</p>');
} else {
  document.write('<p>Sorry, the number was ' + randomNumber + '</p>');
}
