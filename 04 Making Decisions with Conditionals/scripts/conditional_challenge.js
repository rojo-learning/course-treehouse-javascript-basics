
/**
 * Conditional Challenge
 *
 * + Ask at least five questions
 * + Keep track of the number of questions the user answered correctly
 * + Provide a final message after the quiz letting the user know the number of
 *   questions he or she got right.
 * + Rank the player. If the player answered all five correctly, give that
 *   player the gold crown: 3-4 is a silver crown; 1-2 correct answers is a
 *   bronze crown and 0 correct is no crown at all.
 */

var questionsLeft = 5;
var correctAnswers = 0;
var userAnswer;

var answer1 = 'steel';
var answer2 = 'dark';
var answer3 = 'fairy';
var answer4 = 'solgaleo';
var answer5 = 'lunala';


var question1 = 'What "physical" type was introduced on the II generation?';
var question2 = 'What "special" type was introduced on the II generation?';
var question3 = 'What type was introduced on the VI generation?';
var question4 = 'What is the name of the main legendary on the Sun version?';
var question5 = 'What is the name of the main legendary on the Moon version?';

alert(
  'Welcome to the Pokémon Know-It-All!!\n\n' +
  'You\'ll answer a set of ' + questionsLeft + ' questions to show how much you know about the' +
  ' world of Pokémon.\n\n' + 'Ready? GO!'
);

userAnswer = prompt(question1 + '\n(' + (questionsLeft - 1) + ' questions left)');
questionsLeft -= 1;
if (userAnswer.toLowerCase() == answer1) { 
  alert("You're right!");
  correctAnswers += 1;
} else {
  alert('Sorry, the answer is ' + answer1);
};

userAnswer = prompt(question2 + '\n(' + (questionsLeft - 1) + ' questions left)');
questionsLeft -= 1;
if (userAnswer.toLowerCase() == answer2) { 
  alert("You're right!");
  correctAnswers += 1;
} else {
  alert('Sorry, the answer is ' + answer2);
};

userAnswer = prompt(question3 + '\n(' + (questionsLeft - 1) + ' questions left)');
questionsLeft -= 1;
if (userAnswer.toLowerCase() == answer3) { 
  alert("You're right!");
  correctAnswers += 1;
} else {
  alert('Sorry, the answer is ' + answer3);
};

userAnswer = prompt(question4 + '\n(' + (questionsLeft - 1) + ' questions left)');
questionsLeft -= 1;
if (userAnswer.toLowerCase() == answer4) { 
  alert("You're right!");
  correctAnswers += 1;
} else {
  alert('Sorry, the answer is ' + answer4);
};

userAnswer = prompt(question5 + '\n(' + (questionsLeft - 1) + ' questions left)');
questionsLeft -= 1;
if (userAnswer.toLowerCase() == answer5) { 
  alert("You're right!");
  correctAnswers += 1;
} else {
  alert('Sorry, the answer is ' + answer5);
};

if (correctAnswers == 5) {
  document.write('<p>You obtained the Gold Crown!</p>');
} else if (correctAnswers >= 3) {
  document.write('<p>You obtained the Silver Crown!</p>');
} else if (correctAnswers >= 1) {
  document.write('<p>You obtained the Bronze Crown!</p>');
} else {
  document.write('<p>Mmm... How about you study and try harder the next time?</p>');
};
