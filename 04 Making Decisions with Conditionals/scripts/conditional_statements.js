
/**
 * Conditional Statements
 * Control flow or conditional statements allow programs to respond to different situations.
 */

/**
 * Comparison Operators
 * The hear of conditional statements. A true false comparison that determines with code the program
 * should run.
 * 
 * ==   Equal To (Compares only the value)
 * ===  Strict Equal To (Compares the type and the value)
 * >    Greater Than
 * <    Less Than
 * >=   Greater Than Or Equal To
 * <=   Less Than Or Equal To
 * !=   Not Equal To (Compares only the value)
 * !==  Strict Not Equal To (Compares the type and the value)
 */

/**
 * Boolean Values
 * These are just `true` and `false` values.
 */

/**
 * If-Else Statements
 * The combination of if/else statements allows to select 1 or more possible outcomes.
 *
 * if ( condition ) {
 *   // result
 * }
 * 
 * if ( condition ) {
 *   // result 1
 * } else { 
 *   // result 2
 * }
 * 
 * if ( condition ) {
 *   // result 1
 * } else if ( condition ) {
 *   // result 2
 * } else { 
 *   // result 3
 * }
 * 
 */

/**
 * Logical Operators
 * The OR (||) and the AND (&&) logical operators allow to combine multiple tests into a single condition.
 *
 * The OR operator returns true if any of the conditions evaluates to true.
 * if ( condition || condition )
 *
 * The AND operator returns true only if all conditions evaluate to true.
 * if ( condition && condition )
 *
 * This way, several conditions can be chained on a single line.
 * if ( condition && condition || condition... )
 */
