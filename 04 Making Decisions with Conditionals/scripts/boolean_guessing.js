
var correctGuess = false;
var randomNumber = Math.floor(Math.random() * 6) + 1;
var guess = parseInt(prompt('I\'m thinking of a number between 1 and 6.\nWhat is it?'));

if (randomNumber == guess) {
  correctGuess = true;
};

if ( correctGuess ) {
  document.write('<p>Your guessed the number!</p>');
} else {
  document.write('<p>Sorry, the number was ' + randomNumber + '</p>');
};
