
var secondsPerMin = 60;
var minPerHour = 60;
var hoursPerDay = 24;
var daysPerWeek = 7;
var weeksPerYear = 52;
var secondsPerDay = secondsPerMin * minPerHour * hoursPerDay;
var yearAlive = 35;
var secondsAlive = 35 * weeksPerYear * daysPerWeek * secondsPerDay;

document.write('<p>There are ' + secondsPerDay + ' seconds in a day.</p>');
document.write('<p>I\'ve been alive for more than ' + secondsAlive + ' seconds.</p>');
