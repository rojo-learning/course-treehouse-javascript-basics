
/**
 * Numbers
 * JavaScript works with 3 basic number types:
 *   Integers: 5, 0, -100, 9999
 *   Floats: 3.14, -9.888888, .000009
 *   Scientific: 9e-6, 9e+6
 */

var age = 35;
var wage = 7;

/**
 * Operations
 * As other languages, JavaScript allows to perform arithmetic operations with numbers: addition,
 * subtraction, multiplication and division, among others. Using parenthesis the order of the
 * equations can be specified as in math.
 */

age = age - 10;
wage *= 4;

/**
 * Numbers and Strings
 * Values retrieved from web forms will be treated as text by JavaScript. To work with them as
 * numbers, the functions parseInt() and parseFloat() can be used to obtain the numeric value from a
 * string.
 */

var str = '49 steps';
var num = parseInt(str); // => 49

str = '102.99%';
num = parseFloat(str); // => 102.99

/**
 * The Math Object
 * This object is contains functions to perform advanced mathematical operations. As other objects,
 * is also contains useful constants a properties.
 *
 * The complete list of functions can be reviewed on:
 * https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Math
 */

// The constant number PI
var pi = Math.PI; // => 3.141592653589793

// The function to round numbers
// (.5 or below gets truncated, .6 or above is elevated to the next integer)
var rounded = Math.round(pi); // => 3

// The function to truncate numbers
// (Returns the integer part of a float number)
var truncated = Math.trunc(pi); // => 3

// Get the nearest smaller integer to a float number
var floor = Math.floor(pi); // => 3

// Get the next bigger integer to a float number
var ceil = Math.ceil(pi); // => 4

// Get a pseudo-random number
var random = Math.random(); // => A float number between 0 and 1
