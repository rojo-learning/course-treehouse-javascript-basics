
/**
* Instructions
*
* 1. Create a variable capturer input.
* 2. Create a separate variable for each piece of input.
* 3. On each user input, display how many questions are left.
* 4. Add an alert to tell the visitor that they're finished.
* 5. Combine the input with other strings to create a message.
* 6. Print the story to the browser window.
*/

var questions = 3;
var remainingQuestions = ' (' + questions + ' questions left)';

var name = prompt('Hello! What is your name?' + remainingQuestions);

var story = "<div class='container'><p>Nice to meet you, " + name + ".</p>";
story += "<p>Welcome to Alola. This world is inhabited with creatures called \
          pokémon. Humans and pokémon share their lives, creating bonds \
          and achieving great feats.</p>";

document.write(story);

questions = 2;
remainingQuestions = ' (' + questions + ' questions left)';

var type = prompt('What pokémon type is your favorite?' + remainingQuestions);

story = "<p>On these islands, kids of your age start a travel trough each \
         island, attemping tests from the Kahuna that help them to learn \
         and growth in the company of their beloved pokemon.</p>";
story += "<p>If you participate, you may meet several pokémon of the " + type +
           " type, among others. Ain't that cool?</p>"

document.write(story);

questions = 1;
remainingQuestions = ' (' + questions + ' questions left)';

var region = prompt('From what region are you from?' + remainingQuestions);

story = "<p>So you came all the way from " + region + ". You'll find that going \
        around Alola may not seem simple if you don't have a boat, but once you \
        learn about Poké Ride, you'll be able to explore the region more \
        freely.</p>"
story += "<p>Good luck and enjoy your experiences along with Pokémon!</p></div>"

document.write(story);
