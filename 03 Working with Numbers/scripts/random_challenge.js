
var random = 0;
var upperLimit = 0;
var lowerLimit = 0;

alert('Lets get a number between your limits!');

lowerLimit = parseInt(prompt('What is the smallest number you want to get?'));
upperLimit = parseInt(prompt('What is the biggest number you want to get?'));

random = Math.floor(Math.random() * (upperLimit - lowerLimit + 1)) + lowerLimit;

document.write('<p>Your upper limit: ' + upperLimit);
document.write('<p>Your lower limit: ' + lowerLimit);
document.write('<p>The random number: <strong>' + random + '</strong>');
