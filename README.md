
# Treehouse - JavaScript Foundations #

This repository contains notes and practice examples from **JavaScript Basics**, imparted by Dave McFarland at [Threehouse][JF].

> JavaScript is a programming language that drives the web: from front-end user interface design, to backend server-side programming, you'll find JavaScript at every stage of a web site and web application. In this course, you'll learn the fundamental programming concepts and syntax of the JavaScript programming language.

## Contents ##

- **Introducing JavaScript**: Learn about the most popular scripting language on the planet. The language that drives millions of interactive web sites, powers fast web servers and can even be used to build desktop applications. In this stage, you'll create your first JavaScript program and learn how to troubleshoot your programming mistakes.
- **Storing and Tracking Information with Variables**: Learn how to use variables to store information that changes during a program, like the score in a game, or a sales total. You'll also learn about different data types in JavaScript like string, numbers and boolean values.
- **Working With Numbers**: Numbers are everywhere in programming. You use them to track a player's score in a game, to calculate the cost of shipping a product, or just to count the number of times a "Like" button was clicked on a page. In this section of the course, you'll learn how to do basic math in JavaScript.
- **Making Decisions with Conditional Statements**: Conditional statements let you control the "flow" of your program. They let you run different code based on conditions in your program.
- **Creating Reusable Code with Functions**: JavaScript functions are a powerful tool that let you create re-usable chunks of code. They make programming faster, easier and less error-prone. They are also one of the most important concepts in JavaScript programming.

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.

[JF]: http://teamtreehouse.com
