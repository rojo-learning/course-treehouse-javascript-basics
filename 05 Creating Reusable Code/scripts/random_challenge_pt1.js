
function getRandomNumber( lower, upper ) {
  return Math.floor(Math.random() * (upper - lower + 1)) + lower;
};

document.write( "<p>Random number between 1 and 10: " + getRandomNumber( 1, 10) );
document.write( "<p>Random number between 11 and 20: " + getRandomNumber(11, 20) );
document.write( "<p>Random number between 21 and 30: " + getRandomNumber(21, 30) );
document.write( "<p>Random number between 31 and 40: " + getRandomNumber(31, 40) );
