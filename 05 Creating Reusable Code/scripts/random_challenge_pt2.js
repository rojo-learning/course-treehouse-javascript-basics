
function getRandomNumber( lower, upper ) {
  if ( isNaN(lower) || isNaN(upper) ) {
    if ( isNaN(lower) ) { throw new Error('"' + lower + '" is not a number.'); };
    if ( isNaN(upper) ) { throw new Error('"' + upper + '" is not a number.'); };
  }

  return Math.floor(Math.random() * (upper - lower + 1)) + lower;
};

document.write( "<p>Random number between 1 and 10: " + getRandomNumber( 1, 10) );
document.write( "<p>Random number between 11 and 20: " + getRandomNumber('eleven', 20) );
document.write( "<p>Random number between 21 and 30: " + getRandomNumber(21, 30) );
document.write( "<p>Random number between 31 and 40: " + getRandomNumber(31, 'forty') );
