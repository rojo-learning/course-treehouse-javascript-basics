
/**
 * Creating Reusable Code with Functions
 * 
 * Functions allow to create groups of reusable instructions, making reducing
 * the lines of code when the same operations are required in multiple times.
 */

// Basic Structure
function myFunction() {
  // instructions
}

// Calling a Function
myFunction();

// Function Expression
// An anonymous function can be referenced by a variable name, much like and
//  object instantiation:
var aFunction = function () {
  // instructions
}

aFunction();

/**
 * Getting Information From a Function
 *
 * Functions can return values to be used in other parts of the program. For
 * this, the `return` keyword is used.
 *
 * The `return` statement ends the execution of the instructions on a function
 * so it should be used only when all the necessary instructions are executed.
 */

function giveMeFive() {
  return 5;
}

console.log( giveMeFive() );

/**
 * Giving Information to Functions
 *
 * A function can receive information as arguments. The parameters will be
 * available as variables inside the function.
 *
 * function myFunction(arg1, arg2, argn, ...) { };
 *
 * To make functions easy to use, they should receive at most 3 to 4 arguments.
 */

function calculateArea( width, length, unit ) {
  return width * length + ' ' + unit;
};

calculateArea( 5, 4, 'sq ft' ); // => 20 sq ft
calculateArea( 30, 4, 'sq mt' ); // => 120 sq mt

/**
 * Variable Scope
 *
 * Variables defined outside of a function belong to the global scope, while the
 * ones defined inside functions belong to the scope of the function.
 *
 * The variables on the global scope can be accessed by using its name without
 * `var` inside functions, but it is not a good practice. It almost always
 * better to pass the values to a function as parameters.
 */

/**
 * Trowing Errors
 *
 * The function can be instructed to look for wrong data passed to them and to
 * throw errors on that happens.
 */

throw new Error('You have reached the end of the file');
